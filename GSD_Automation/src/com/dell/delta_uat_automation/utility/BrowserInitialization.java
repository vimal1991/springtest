package com.dell.delta_uat_automation.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;



public class BrowserInitialization {

Hashtable<String,String> runMode;
public WebDriver driver=null;	
	
Properties obj_property;

@BeforeSuite
public void  loadObjectsPropertiesFromExcel()
{		
	
}

	@BeforeClass
	public  void setBrowser() throws IOException {
    	//System.out.println("Fails here 0");
		
    obj_property=CommonUtility.loadPropertyFile();
 
    //System.out.println("Fails here 1");
    	
    //System.out.println(obj_property.getProperty("FireDriverExePath"));
    System.out.println(obj_property.getProperty("setPropertyArg1ForFireDriver"));
    //String path =	obj_property.getProperty("FireDriverExePath");

    	
    //System.setProperty(obj_property.getProperty("setPropertyArg1ForFireDriver"), obj_property.getProperty("FireDriverExePath"));
    	
   	System.setProperty("webdriver.gecko.driver","/var/lib/jenkins/workspace/projectFunctionalTesting/GSD_Automation/data/geckodriver");
   	//System.setProperty("webdriver.gecko.driver","/home/hashworks/Raj/DevOps/SCDPOC/QA_Code/GSD_Automation/data/geckodriver");


   // System.out.println("driver path loaded");
   	
	DesiredCapabilities obj_DesiredCapabilities = DesiredCapabilities.firefox();
	
	//obj_DesiredCapabilities.setCapability(firebox, true);;
	/*
	//obj_DesiredCapabilities.setCapability(FirefoxDriver.,true); 
	obj_DesiredCapabilities.setCapability("nativeEvents", false);    
	obj_DesiredCapabilities.setCapability("unexpectedAlertBehaviour", "accept");
	obj_DesiredCapabilities.setCapability("ignoreProtectedModeSettings", true);
	obj_DesiredCapabilities.setCapability("disable-popup-blocking", true);
	obj_DesiredCapabilities.setCapability("enablePersistentHover", true);
	obj_DesiredCapabilities.setCapability("ignoreZoomSetting", true);*/
	
	//System.out.println("Fails here 2");
	
	//System.out.println(obj_property.getProperty("ge1url"));
	//System.out.println("fails here");
	driver = new FirefoxDriver();
	//driver.get("http://13.126.53.170:8080/");
	
	//System.out.println("Fails here 3");
	
	//driver.get("https://www.google.co.in/");
	
	//driver.manage().window().maximize();
	
	if (TestNGCreator.envi.equalsIgnoreCase("ge1")) {
		driver.get(obj_property.getProperty("ge1url"));
		System.out.println(obj_property.getProperty("ge1url"));
	} else if (TestNGCreator.envi.equalsIgnoreCase("ge2")) {
		driver.get(obj_property.getProperty("ge2url"));
		System.out.println(obj_property.getProperty("ge2url"));
	} else if (TestNGCreator.envi.equalsIgnoreCase("ge3")) {
		driver.get(obj_property.getProperty("ge3url"));
		System.out.println(obj_property.getProperty("ge3url"));
	} else if (TestNGCreator.envi.equalsIgnoreCase("ge4")) {
		driver.get(obj_property.getProperty("ge4url"));
		System.out.println(obj_property.getProperty("ge4url"));
	} else if (TestNGCreator.envi.equalsIgnoreCase("prod")) {
		driver.get(obj_property.getProperty("produrl"));
		System.out.println(obj_property.getProperty("produrl"));
	} 
	//System.out.println("Fails here 3");
	}
	
	
	
	
	
	@AfterMethod
	public void activitiesAfterExecutingTC(ITestResult testResult){
		
		
		
		
		try {
			
			
			if (testResult.getStatus() == ITestResult.FAILURE) { 
            Path path = Paths.get(TestNGCreator.finalResultPath+"\\"+TestNGCreator.tcID+"_"+TestNGCreator.region+"_01_ScreenShots");
            Files.createDirectories(path);
            
           
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            // The below method will save the screen shot in d drive with name
            // "screenshot.png"
            FileUtils.copyFile(scrFile,
                         new File(path+"\\"+TestNGCreator.tcID+"_"+TestNGCreator.region+"_ScreenShot"+".png"));
            
            //+new SimpleDateFormat("yyyy-MMM-dd(hh:mm:ss)").format(Calendar.getInstance().getTime())
            
            /*FileUtils.copyFile(scrFile,
                    new File("C:\\Users\\Narasimhamurthy_SJ\\Desktop\\Temp\\GSD_Automation\\Object_Data_Repository\\1342105_AMER_01_ScreenShots\\screenshot"+ ".jpg"));
       
       */     
			}
     } catch (Exception e) {
            // TODO Auto-generated catch block
    	 System.out.println("Failed while taking screen shot"+e);
            e.printStackTrace();
     }

	}
	
	//close the browser
	@AfterSuite
	public void terminateSettings() {
		driver.quit();
		Report.header();
		Report.closure();
	}

}
