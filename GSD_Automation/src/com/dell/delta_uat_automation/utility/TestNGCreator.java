/*package com.dell.delta_uat_automation.utility;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.testng.TestNG;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.internal.ClassHelper;
import org.testng.internal.PackageUtils;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

@Listeners(com.dell.delta_uat_automation.utility.ListenerClass.class)
public class TestNGCreator {
	
	public static Class className = null;
	public static String stringClass = null;
	public static String excelPath = null;
	public static String sheetName = null;
	public static String resultFileName = null;
	public static ArrayList<String> TestData = null;
	

	public static void main(String[] args) {
		TestNGCreator test = new TestNGCreator();
		
		System.out.println(TestData);
		excelPath = args[2];
		sheetName = args[3];
		resultFileName = args[4];
		
		TestData = ReadExcel.getTestData(args[2], args[3], "Test Case ID", "Region", args[0], args[1]);
		test.createXml(args[0], args[1]);
		System.out.println("class name :"+stringClass);
//		TestNGCreator.createXml(args[0], args[1], args[2]);
		
//		testcaseID, region, excelPath, sheetName, finalReportPath. 
//		Report format: 
	}
	public void createXml(String testCaseId, String region)  
	{
		try{
			
		String testName = testCaseId + "_" + region;
		List<String> testNames = new ArrayList<String>();
//		com.dell.delta_uat_automation.testcases
		String[] testClasses = PackageUtils.findClassesInPackage("com.dell.delta_uat_automation.testcases", new ArrayList<String>(),
				new ArrayList<String>());
		List<XmlInclude> includes = new ArrayList<XmlInclude>();

//		Class className = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		for (String eachClass : testClasses) {
			
//			System.out.println("eachClass   :"+eachClass);
			Class currentClass = cl.loadClass(eachClass);

			if (eachClass.contains(testCaseId)) {
				className = currentClass;
				stringClass = currentClass.getSimpleName();
				Set<Method> allMethods = ClassHelper.getAvailableMethods(currentClass);
				Iterator<Method> iMethods = allMethods.iterator();
				while (iMethods.hasNext()) {
					Method eachMethod = iMethods.next();
					Test test = eachMethod.getAnnotation(Test.class);
					if (test != null) {
//						System.out.println("Method Name = " + eachMethod.getName());
						String method = eachMethod.getName();
						if(method.contains(testName))
						{
							includes.add(new XmlInclude(method));
						}
						

						// System.out.println("Its class = " +
						// eachMethod.getDeclaringClass().getName());
					}
				}

			} else {
				continue;
			}

		}
		if(!includes.isEmpty())
		{
			 List<Class> listnerClasses = new ArrayList<Class>();
			 listnerClasses.add(com.dell.delta_uat_automation.utility.ListenerClass.class);
			TestNG testNG = new TestNG();
			
			XmlSuite suite = new XmlSuite();
			suite.setName("Suite");
			XmlTest xmlTest = new XmlTest(suite);
			testNG.setUseDefaultListeners(true);
			testNG.setListenerClasses(listnerClasses); 
			xmlTest.setName("Test");
			xmlTest.setVerbose(0);
			XmlClass xmlClass = new XmlClass(className);
			xmlTest.getClasses().add(xmlClass);
			XmlInclude include = new XmlInclude();
			xmlClass.setIncludedMethods(includes);
			testNG.setXmlSuites(Arrays.asList(suite));
			FileWriter writer = new FileWriter(new File("TestNG.xml"));
			writer.write(suite.toXml());
			writer.flush();
			writer.close();
			testNG.setUseDefaultListeners(true);
			testNG.run();
		}
//		else {
//			System.out.println("null");
//			
//		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
*/


 

package com.dell.delta_uat_automation.utility;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.testng.ITestNGListener;
import org.testng.TestNG;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.internal.ClassHelper;
import org.testng.internal.PackageUtils;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

@Listeners(com.dell.delta_uat_automation.utility.ListenerClass.class)
public class TestNGCreator extends ListenerClass{
               
           
  
	            public static Class className = null;
	           	public static String stringClass = null;
	           	public static String excelPath = null;
	           	public static String sheetName = null;
	           	public static String finalResultPath = null;
	           	public static ArrayList<String> TestData = null;
	           	public static String folderPath = null;
	           	public static String configfolder = null;
	           	public static String tcID = null;
	           	public static String region = null;
	           	public static String envi = null; 

               
               public static void main(String[] args) {
		            	tcID = args[0];
		           		region = args[1];
		           		envi = args[2];
		           		excelPath = args[3];
		           		//sheetName = args[4];
		           		finalResultPath = args[4];
		
		           		TestNGCreator test = new TestNGCreator();
		           		Path p = Paths.get(excelPath);
		           		Path orfolder = p.getParent();
		/*
		           		if (envi.equalsIgnoreCase("Ge1")) {
		           			folderPath = orfolder.toString() + "\\GE1ObjectRepository.xlsx";
		           		} else if (envi.equalsIgnoreCase("Ge2")) {
		           		s	folderPath = orfolder.toString() + "\\GE2ObjectRepository.xlsx";
		           		} else if (envi.equalsIgnoreCase("Ge3")) {
		           			folderPath = orfolder.toString() + "\\GE3ObjectRepository.xlsx";
		           		} else if (envi.equalsIgnoreCase("Ge4")) {
		           			folderPath = orfolder.toString() + "\\GE4ObjectRepository.xlsx";
		           		} else if (envi.equalsIgnoreCase("prod")) {
		           			folderPath = orfolder.toString() + "\\PRODObjectRepository.xlsx";
		           		}*/
		
		           		configfolder = orfolder.toString() + "/Config.properties";
		
		           		//TestData = ReadExcel.getTestData(excelPath, "Test Case ID", "Region", tcID, region);
		           		//ReportClass.header();
		           		test.createXml(tcID, region);
		           		System.out.println("class name :" + stringClass); 
               }
               
               public void createXml(String testCaseId, String region)  
               {
            	   boolean flag = false;
            	   String includeName = null;
                              try{
                                             
                              String testName = testCaseId + "_" + region;
                              List<String> testNames = new ArrayList<String>();
//                           com.dell.delta_uat_automation.testcases
                              String[] testClasses = PackageUtils.findClassesInPackage("com.dell.delta_uat_automation.testcases", new ArrayList<String>(),
                                                            new ArrayList<String>());
                              List<XmlInclude> includes = new ArrayList<XmlInclude>();

//                           Class className = null;
                              ClassLoader cl = Thread.currentThread().getContextClassLoader();
                              for (String eachClass : testClasses) {
                                             
//                                          System.out.println("eachClass   :"+eachClass);
                                             Class currentClass = cl.loadClass(eachClass);

                                             if (eachClass.contains(testCaseId)) {
                                                            className = currentClass;
                                                            stringClass = currentClass.getSimpleName();
                                                            Set<Method> allMethods = ClassHelper.getAvailableMethods(currentClass);
                                                            Iterator<Method> iMethods = allMethods.iterator();
                                                            while (iMethods.hasNext()) {
                                                                           Method eachMethod = iMethods.next();
                                                                           Test test = eachMethod.getAnnotation(Test.class);
                                                                           if (test != null) {
//                                                                                       System.out.println("Method Name = " + eachMethod.getName());
                                                                                          String method = eachMethod.getName();
                                                                                          String method_region = method.split("_")[3];
                                                                                          if(method_region.equals(region))
                                                                                          {
//                                                                                        	  System.out.println("region"+method_region +" "+stringClass);
                                                                                        	 includeName=method; 
                                                                                                         includes.add(new XmlInclude(method));
//                                                                                                         System.out.println("mtd name"+method);
                                                                                                         flag = true; 
                                                                                          }

                                                                                          // System.out.println("Its class = " +
                                                                                          // eachMethod.getDeclaringClass().getName());
                                                                           }
                                                            }

                                                            if(flag)
                                                            {
                                                            	
                                                            	break;
                                                            }
                                             }

                              }
                              if(!includes.isEmpty())
                              {
                                             List<Class<? extends ITestNGListener>> listnerClasses = new ArrayList<Class<? extends ITestNGListener>>();
                                             listnerClasses.add(com.dell.delta_uat_automation.utility.ListenerClass.class);
                                             TestNG testNG = new TestNG();
                                             
                                             XmlSuite suite = new XmlSuite();
                                             suite.setName("Suite");
                                             XmlTest xmlTest = new XmlTest(suite);
//                                             testNG.setUseDefaultListeners(true);
                                             testNG.setListenerClasses(listnerClasses); 
//                                             testNG.set
                                             xmlTest.setName("Test");
                                             xmlTest.setVerbose(0);
                                             XmlClass xmlClass = new XmlClass(className);
                                             xmlTest.getClasses().add(xmlClass);
                                             XmlInclude include = new XmlInclude(includeName);
                                             xmlClass.setIncludedMethods(includes);
                                             testNG.setXmlSuites(Arrays.asList(suite));
//                                             FileWriter writer = new FileWriter(new File("TestNG.xml"));
//                                             writer.write(suite.toXml());
//                                             writer.flush();
//                                             writer.close();
//                                             testNG.setUseDefaultListeners(true);
                                             testNG.run();
                              }
//                           else {
//                                          System.out.println("null");
//                                          
//                           }
                              }
                              catch(Exception e)
                              {
                                             e.printStackTrace();
                              }
               }
}
