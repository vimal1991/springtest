package com.dell.delta_uat_automation.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.dell.delta_uat_automation.utility.TestNGCreator;
import com.dell.delta_uat_automation.utility.Ui;

public class TC_11_APJ_POC_App_Test extends BaseTest {

	@Test
	public void validate_TC_11_APJ_POC_App_Test() {
		try {
			//obj_Generic_WorkFlows.TestGoogle(driver);
			obj_Generic_WorkFlows.AppOpen(driver);
			obj_Generic_WorkFlows.AppLogin(driver);
			obj_Generic_WorkFlows.AppLogout(driver);


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Failed while executing test case TC_Google_Sign_In--------->\n  ",e);
		}

		finally {

			driver.quit();
		}

	}

}
