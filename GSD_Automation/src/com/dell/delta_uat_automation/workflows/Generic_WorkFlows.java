package com.dell.delta_uat_automation.workflows;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.dell.delta_uat_automation.utility.Report;

public class Generic_WorkFlows extends BaseFlow {
	
	public void TestGoogle(WebDriver driver)
	{
		driver.findElement(By.xpath("//a[text()='Sign in']")).click();
		System.out.println("Clicked on the Sign In Link");
		
	}
	
	public void AppOpen(WebDriver driver)
	{
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Go to User Welcome Page!']")));
			driver.findElement(By.xpath("//a[text()='Go to User Welcome Page!']")).click();
			Report.put("User gets into the application page' s welcome page", "User should be able to get into the application's welcome page",
					"User gets into the application's welcome page successfully", "PASS");
		}
		catch (Exception e){
			Report.put("User gets into the application's welcome page", "User should be able to get into the application's welcome page",
					"User did not get into the application's welcome page successfully", "FAIL");
			Assert.fail("Caught Problem to open the URL :", e);	
		}
		
	}
	
	public void AppLogin(WebDriver driver) throws InterruptedException
	{
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
	
		try{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='username']")));
			driver.findElement(By.xpath("//input[@name='username']")).sendKeys("peter");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='password']")));
			driver.findElement(By.xpath("//input[@name='password']")).sendKeys("peter");
			
			driver.findElement(By.xpath("html/body/form/div[3]/input")).click();		
			boolean bool1 = driver.findElement(By.xpath("html/body/h1")).isDisplayed();	
		
			if(bool1==true) {
				System.out.println("loggged in successfully");
				Report.put("User logs into the application", "User should be able to login into the application","User logins into the application successfully", "PASS");
			}
		}
		catch (Exception e){
			System.out.println("Error in Login");
			Report.put("User logs into the application", "User should be able to login into the application","User did not login into the application successfully", "FAIL");
			Assert.fail("Caught Problem during the login:", e);	
		}		
	}
	
	public void AppLogout(WebDriver driver)
	{
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/form/input[1]")));
			driver.findElement(By.xpath("html/body/form/input[1]")).click();
			boolean bool2 =  driver.findElement(By.xpath("html/body/div[1]/h1")).isDisplayed();
			if (bool2==true){
				System.out.println("Log out is successful");
				Report.put("User logs out from the application", "User should be able log out from the application","User logs out from the application successfully", "PASS");
			}
		}
		catch (Exception e){
			System.out.println("Error in Log out");
			Report.put("User logs out from the application", "User should be able to logout from the application","User did not log out from the application successfully", "FAIL");
			Assert.fail("Caught Problem during the logout:", e);	
		}

	}
}
